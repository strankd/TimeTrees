using System;
using System.Globalization;

namespace TimeTrees.Core.WorkWithDateTools
{
    public static class DateHelper
    {
        public static DateTime ParseDate(string value)
        {
            DateTime date;
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(value, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    { 
                        throw new Exception("Неверный формат даты");
                    }
                }
            }
            return date;
        }

        public static DateTime CheckBirthDateCorectly(string birthDate)
        {
            DateTime dateTime;
            do
            {
                try
                {
                    if (birthDate.Length != 10)
                        throw new Exception("Введенная дата не полная, введите полную дату: ");
                    dateTime = ParseDate(birthDate);
                    break;
                }
                catch (Exception e)
                { 
                    Console.Write(e.Message);
                    birthDate = Console.ReadLine();
                }
            } while (true);

            return dateTime;
        }

        public static DateTime? CheckDeathDateCorectly(string deathDate, DateTime birthDate)
        {
            DateTime? dateDeath;
            
            do
            {
                if (deathDate == "-" || string.IsNullOrEmpty(deathDate))
                {
                    dateDeath = null;
                    break;
                }
                else
                    dateDeath = ParseDate(deathDate);
                try
                {
                    if (!string.IsNullOrEmpty(deathDate))
                    {
                        if (dateDeath < birthDate)
                            throw new Exception("Дата смерти не может быть раньше даты рождния");
                        if (deathDate.Length != 10)
                            throw new Exception("Дата смерти введена не полностью");
                    }
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.Write("Введите корректную дату или нажмите Enter для продолжния: ");
                    deathDate = Console.ReadLine();
                }
            } while (true);
            return dateDeath;
        }
    }
}