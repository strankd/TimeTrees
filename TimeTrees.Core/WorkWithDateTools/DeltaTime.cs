using System;
using System.Collections.Generic;

namespace TimeTrees.Core.WorkWithDateTools
{
    public class DeltaTime
    {
        public readonly int Years;
        public readonly int Months;
        public readonly int Days;

        
        private (int, int, int) DeltaMinAndMaxDate(List<TimelineEvent> timelinelist)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timelinelist);
            DateTime diffdate = DateTime.MinValue.Add(maxDate.Subtract(minDate));
            return (diffdate.Year - DateTime.MinValue.Year, diffdate.Month - DateTime.MinValue.Month,
                diffdate.Day - DateTime.MinValue.Day);
        }
        private (DateTime, DateTime) GetMinAndMaxDate(List<TimelineEvent> timeLineList)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            for(int i = 0; i < timeLineList.Count;i++)
            {
                DateTime date = timeLineList[i].Date;
                if (date < minDate) minDate = date;
                if (date > maxDate) maxDate = date;
            }
            return (minDate, maxDate);
        }
        
        public DeltaTime(List<TimelineEvent> timelinelist)
        {
            (Years, Months, Days) = DeltaMinAndMaxDate(timelinelist);
        }
    }
}