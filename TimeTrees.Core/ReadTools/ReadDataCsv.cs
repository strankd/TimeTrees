using System.Collections.Generic;
using System.IO;
using TimeTrees.Core.Models;
using TimeTrees.Core.WorkWithDateTools;
using TimeTrees.Core.WriteTools;

namespace TimeTrees.Core.ReadTools
{
    public class ReadDataCsv : IReadFile
    {
        private const int PersonIndex = 0;
        private const int NameIndex = 1;
        private const int FirstParent = 2;
        private const int SecondParent = 3;
        private const int BirthIndex = 4;
        private const int DeathIndex = 5;

        private const int EventId = 0;
        private const int DateEvent = 1;
        private const int EventIndex = 2;
        
        public List<Person> ReadPersonFile()
        {
            string[] people = File.Exists("../../../../files/people.csv")
                ? File.ReadAllLines("../../../../files/people.csv")
                : GenerateTestCsvFile.GeneratePeopleFileCsv();
            List<Person> peopleList = new List<Person>();
            for (int i = 0;i < people.Length; i++)
            {
                var line = people[i];
                string[] parts = line.Split(";");
                Person elementPeople = new Person{
                    Id = int.Parse(parts[PersonIndex]),
                    Name = parts[NameIndex],
                    BirthDate = DateHelper.ParseDate(parts[BirthIndex]),
                };
                if(parts.Length > 5)
                    elementPeople.DeathDate = DateHelper.ParseDate(parts[DeathIndex]);
                else
                    elementPeople.DeathDate = null;
                peopleList.Add(elementPeople);
            }

            peopleList = AddParent(peopleList, people);
            return peopleList;
        }

        private static List<Person> AddParent(List<Person> list, string[] people)
        {
            for (int i = 0; i < people.Length; i++)
            {
                string[] parts = people[i].Split(";");
                if (parts[FirstParent] != "0")
                    list[i].FirstParent = list[int.Parse(parts[FirstParent]) - 1];
                if (parts[SecondParent] != "0")
                    list[i].SecondParent = list[int.Parse(parts[SecondParent]) - 1];
            }
            return list;
        }

        public List<TimelineEvent> ReadTimelineEventsFile()
        {
            string[] timeline = File.Exists("../../../../files/timeline.csv") 
                ? File.ReadAllLines("../../../../files/timeline.csv")
                : GenerateTestCsvFile.GenerateTimeLineFileCsv();
            List<TimelineEvent> timelineList = new List<TimelineEvent>();
            for (int i = 0; i < timeline.Length; i++)
            {
                var line = timeline[i];
                string[] parts = line.Split(";");
                TimelineEvent eventTimeline = new TimelineEvent()
                {
                    Id = int.Parse(parts[EventId]),
                    Date = DateHelper.ParseDate(parts[DateEvent]),
                    Event = parts[EventIndex]
                };
                timelineList.Add(eventTimeline);
            }
            
            return timelineList;
        }
    }
}