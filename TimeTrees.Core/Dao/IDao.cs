using System.Collections.Generic;

namespace TimeTrees.Core.Dao
{
    public interface IDao<T>
    {
        List<T> GetAll();
        T Get(int id);
        void Update(T updatedElem);
        void Add(T element);
        void Delete(int id);
        int NextId();
    }
}