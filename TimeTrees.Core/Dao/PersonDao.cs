using System.Collections.Generic;
using System.Linq;
using TimeTrees.Core.Models;
using TimeTrees.Core.ReadTools;

namespace TimeTrees.Core.Dao
{
    public class PersonDao : IPersonDao
    {
        private static List<Person> _persons;
        public PersonDao()
        {
            ReadDataJson read = new ReadDataJson();
            _persons = read.ReadPersonFile();
        }

        public PersonDao(Format format)
        {
            if (format == Format.Csv)
            {
                ReadDataCsv readDataCsv = new ReadDataCsv();
                _persons = readDataCsv.ReadPersonFile();
            }
            else
            {
                ReadDataJson readDataJson = new ReadDataJson();
                _persons = readDataJson.ReadPersonFile();
            }
        }

        public List<Person> GetAll() => _persons;

        public Person Get(int id) => _persons.First(x => x.Id == id);

        public void Update(Person updatePerson)
        {
            Person person = Get(updatePerson.Id);
            person.Name = updatePerson.Name;
            person.BirthDate = updatePerson.BirthDate;
            person.DeathDate = updatePerson.DeathDate;
        }
        public void Add(Person person)
        {
            person.Id = NextId();
            _persons.Add(person);
        }

        public void Delete(int id)
        {
            var person = Get(id);
            
            _persons.Remove(person);
        }

        public void Delete(int idChild, int idParent)
        {
            var child = Get(idChild);
            if (child.FirstParent != null && child.FirstParent.Id == idParent)
                child.FirstParent = null;
            else
                child.SecondParent = null;
        }
        public int NextId()
        {
            if (!_persons.Any())
                return 1;
            return _persons.Last().Id + 1;
        }

        public void AddParent(Person parent, int id)
        {
            var person = Get(id);
            if (person.FirstParent == null)
                person.FirstParent = parent;
            else
                person.SecondParent = parent;
        }

        public List<Person> GetParent(int id)
        {
            var person = Get(id);
            return new List<Person>()
            {
                person.FirstParent,
                person.SecondParent
            };
        }

        public bool CheckParent(int idChild, int idParent)
        {
            var child = Get(idChild);
            if (child.FirstParent == null && child.SecondParent == null) return false;
            var parent = Get(idParent);
            return child.FirstParent != null
                ? child.FirstParent.Id == parent.Id
                : child.SecondParent.Id == parent.Id;
        }

        public bool CheckCorrectlyAgeParent(int idChild, int idParent)
        {
            var child = Get(idChild);
            var parent = Get(idParent);

            return child.BirthDate < parent.BirthDate;
        }
    }
}