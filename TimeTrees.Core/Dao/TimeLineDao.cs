using System.Collections.Generic;
using System.Linq;
using TimeTrees.Core.ReadTools;

namespace TimeTrees.Core.Dao
{
    public class TimeLineDao : IDao<TimelineEvent>
    {
        private static List<TimelineEvent> _list;

        public TimeLineDao()
        {
            ReadDataJson readData = new ReadDataJson();
            _list = readData.ReadTimelineEventsFile();
        }

        public TimeLineDao(Format format)
        {
            if (format == Format.Csv)
            {
                ReadDataCsv readDataCsv = new ReadDataCsv();
                _list = readDataCsv.ReadTimelineEventsFile();
            }
            else
            {
                ReadDataJson readDataJson = new ReadDataJson();
                _list = readDataJson.ReadTimelineEventsFile();
            }
        }

        public List<TimelineEvent> GetAll() => _list;

        public TimelineEvent Get(int id) => _list.First(x => x.Id == id);

        public void Update(TimelineEvent updatedElem) { }

        public void Add(TimelineEvent element)
        {
            element.Id = NextId();
            _list.Add(element);
        }

        public void Delete(int id) { }

        public int NextId()
        {
            if (!_list.Any())
                return 1;
            return _list.Last().Id + 1;
        }
    }
}