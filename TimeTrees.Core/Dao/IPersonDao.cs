using System.Collections.Generic;
using TimeTrees.Core.Models;

namespace TimeTrees.Core.Dao
{
    public interface IPersonDao : IDao<Person>
    {
        void AddParent(Person parent, int id);
        List<Person> GetParent(int id);
        bool CheckParent(int idChild, int idParent);
        bool CheckCorrectlyAgeParent(int idChild, int idParent);
        void Delete(int idChild, int idParent);
    }
}