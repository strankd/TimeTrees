using System;
using System.Collections.Generic;
using TimeTrees.Core.Models;

namespace TimeTrees.Core
{
    public class TimelineEvent
    {
        public int Id { get; set; }
        public DateTime Date {get; set;}
        public string Event {get; set;}
        public List<Person> Participants { get; set; }
    }
}