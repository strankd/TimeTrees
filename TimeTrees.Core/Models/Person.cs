using System;

namespace TimeTrees.Core.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set;}
        public Person FirstParent { get; set; } 
        public Person SecondParent { get; set; } 
        public DateTime BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }

        public Person() {}

        public Person(string name, DateTime birthDate, DateTime? death)
        {
            Name = name;
            BirthDate = birthDate;
            DeathDate = death;
        }
        public Person(int id,string name, DateTime birthDate, DateTime? death) : this(name, birthDate, death)
        {
            Id = id;
        }
    }
}