using System.Windows.Controls;
using TimeTrees.Core.Dao;
using TimeTrees.Core.Models;
using TimeTrees.DesktopGui.Model;
using System.Windows.Shapes;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.DAO
{
    public interface IRectangleDao : IDao<RectangleModel>
    {
        void Save(Canvas canvas);
        void DeleteConnection(Rectangle rectangleFirst, Rectangle rectangleSecond, ConnectionType type);
        void AddConnect(Rectangle source, Rectangle destination, ConnectionType type);
        bool CheckSpouseExist(int id);
        bool CheckParentCount(int id);
        void AddParent(Person person, int id);
        bool CheckCorrectlyAge(Rectangle child, Rectangle parent);
    }
}