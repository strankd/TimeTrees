using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Shapes;
using TimeTrees.Core.Dao;
using TimeTrees.Core.Models;
using TimeTrees.Core.WriteTools;
using TimeTrees.DesktopGui.Additional_tools;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.DAO
{
    public sealed class RectangleDao : IRectangleDao
    {
        private static List<RectangleModel> _rectangleDao;
        private static IPersonDao _personDao; 

        public RectangleDao()
        {
            _personDao ??= new PersonDao();
            _rectangleDao ??= WorkWithRectangleData.ReadData();
        }

        public List<RectangleModel> GetAll() => _rectangleDao;

        public RectangleModel Get(int id) => _rectangleDao.First(x => x.Id == id);

        public void Update(RectangleModel updatedRectangleModel)
        {
            var rect = Get(updatedRectangleModel.Id);
            rect.Rectangle = updatedRectangleModel.Rectangle;
            rect.Text.Text = $"{((Person) rect.Rectangle.DataContext).Id}) {((Person) rect.Rectangle.DataContext).Name}" +
                        $"\n{((Person) rect.Rectangle.DataContext).BirthDate.ToString("yyyy/MM/dd")}";
            _personDao.Update((Person) rect.Rectangle.DataContext);
        }

        public void Add(RectangleModel newRect)
        {
            var person = (Person) newRect.Rectangle.DataContext;
            _personDao.Add(person);
            newRect.Id = _personDao.Get(person.Id).Id;
            _rectangleDao.Add(newRect);
        }

        public void Save(Canvas canvas)
        {
            foreach (var child in canvas.Children)
            {
                if (child is Rectangle rectangle)
                {
                    var rect = Get(((Person) rectangle.DataContext).Id);
                    rect.CanvasPositionX = Canvas.GetLeft(rectangle);
                    rect.CanvasPositionY = Canvas.GetTop(rectangle);
                }
            }
            WorkWithRectangleData.WriteRectDaoInFile(_rectangleDao);
            WriteData.WritePeopleInJsonFile(_personDao.GetAll());
        }

        public void Delete(int id)
        {
            var model = Get(id);
            if (model.Spouse != 0)
            { 
                var spouseCurrentModel = Get(model.Spouse); 
                spouseCurrentModel.Spouse = 0; 
            }
            if (model.Children != null)
            {
                foreach (var child in model.Children)
                {
                    _personDao.Delete(child, model.Id);
                }
            }

            var parent = _personDao.GetParent(model.Id);
            foreach (var person in parent)
            {
                if(person == null) continue;
                var rectParent = Get(person.Id);
                rectParent.Children.Remove(model.Id);
            }
            _rectangleDao.Remove(model);
            _personDao.Delete(model.Id);
        }

        public void DeleteConnection(Rectangle rectangleFirst, Rectangle rectangleSecond, ConnectionType type)
        {
            var modelRect1 = Get(((Person) rectangleFirst.DataContext).Id);
            var modelRect2 = Get(((Person) rectangleSecond.DataContext).Id);

            if (type == ConnectionType.Spouses)
            {
                modelRect1.Spouse = 0;
                modelRect2.Spouse = 0;
            }
            else
            {
                if (_personDao.CheckParent(modelRect1.Id, modelRect2.Id))
                {
                    modelRect2.Children.Remove(modelRect1.Id);
                    _personDao.Delete(modelRect1.Id,modelRect2.Id);
                }
                else
                {
                    modelRect1.Children.Remove(modelRect2.Id);
                    _personDao.Delete(modelRect2.Id,modelRect1.Id);
                }
                    
            }
        }
        
        public int NextId()
        {
            if (!_rectangleDao.Any())
                return 1;
            return _rectangleDao.Last().Id + 1;
        }

        public void AddConnect(Rectangle source, Rectangle destination, ConnectionType type)
        {
            var rectSource = Get(((Person) source.DataContext).Id);
            var rectDest = Get(((Person) destination.DataContext).Id);
            if (type == ConnectionType.Parent)
            {
                if (rectSource.Children == null)
                {
                    rectSource.Children = new List<int>();
                }
                rectSource.Children.Add(Get(((Person) destination.DataContext).Id).Id);
            }
            else
            {
                if (rectSource.Spouse == 0 && rectDest.Spouse == 0)
                {
                    rectSource.Spouse = Get(((Person) destination.DataContext).Id).Id;
                    rectDest.Spouse = Get(((Person) source.DataContext).Id).Id;
                }
            }
        }

        public bool CheckSpouseExist(int id) => Get(id).Spouse != 0;

        public bool CheckParentCount(int id)
        {
            var person = _personDao.Get(id);
            return person.FirstParent == null || person.SecondParent == null;
        }

        public void AddParent(Person person, int id) => _personDao.AddParent(person, id);

        public bool CheckCorrectlyAge(Rectangle child, Rectangle parent) =>
            _personDao.CheckCorrectlyAgeParent(((Person) child.DataContext).Id, ((Person) parent.DataContext).Id);
    }
}