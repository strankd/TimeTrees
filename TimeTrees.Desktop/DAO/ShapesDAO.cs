﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Shapes;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.DAO
{
    internal class ShapesDao
    {
        private static List<ConnectionRectangle> _rectangleInfos;

        public ShapesDao()
        {
            _rectangleInfos = new List<ConnectionRectangle>();
        }

        public ConnectionType GetConnectionType(Rectangle rectangle, Rectangle destination) => _rectangleInfos.Find(x => x.Source == rectangle && x.Destination == destination || x.Destination == rectangle && x.Source == destination).ConnectionType;
        public bool CheckRectExist(Rectangle sourseRectangle,Rectangle hoveredRectangle) => _rectangleInfos.Any(x => (x.Source == hoveredRectangle && x.Destination == sourseRectangle) ||
            (x.Destination == hoveredRectangle && x.Source == sourseRectangle));
        
        public void AddRectangle(Rectangle rectangle)
        {
            if (_rectangleInfos.Any(x => x.Source == rectangle || x.Destination == rectangle))
            {
                _rectangleInfos.Add(new ConnectionRectangle
                {
                    Source = rectangle
                });
            }
        }

        public void AddConnection(Rectangle sourceRectangle, Rectangle destinationRectangle, Polyline polyline, ConnectionType connectionType)
        {
            bool rectangleInfo = CheckRectExist(sourceRectangle, destinationRectangle);
            
            if (!rectangleInfo)
            {
                _rectangleInfos.Add(new ConnectionRectangle
                {
                    Source = sourceRectangle,
                    Destination = destinationRectangle,
                    Polyline = polyline,
                    ConnectionType = connectionType
                });
            }
        }

        public List<(Rectangle, Polyline)> GetConnections(Rectangle selectedRectangle)
        {
            List<(Rectangle, Polyline)> result = new ();

            result.AddRange(_rectangleInfos
                .Where(x => x.Source == selectedRectangle)
                .Select(x => (x.Destination, x.Polyline))
                .ToList());
                
            result.AddRange(_rectangleInfos
                .Where(x => x.Destination == selectedRectangle)
                .Select(x => (x.Source, x.Polyline))
                .ToList());

            return result;
        }

        public ConnectionRectangle GetConnection(Polyline polyline) => _rectangleInfos.First(x => x.Polyline == polyline);

        public void DeleteRectangle(Rectangle rectangle)
        {
            var rectConnetion = _rectangleInfos.Where(x => x.Source == rectangle || x.Destination == rectangle).ToList();
            foreach (var connection in rectConnetion)
            {
                _rectangleInfos.Remove(connection);
            }
        }

        public void DeletePolyline(Polyline polyline)
        {
            var connection = GetConnection(polyline);
            _rectangleInfos.Remove(connection);
        }
    }
}
