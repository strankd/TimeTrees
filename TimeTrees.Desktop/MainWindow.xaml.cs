﻿using System.Windows;
using TimeTrees.DesktopGui.Additional_tools;
using TimeTrees.DesktopGui.Controllers;
using TimeTrees.DesktopGui.DAO;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.Windows;

namespace TimeTrees.DesktopGui
{
    public partial class MainWindow : Window
    {
        private readonly Tools _tools;
        private Controller _currentController;
        public ViewRectangleInfo ViewRectangleInfo { get; set; }
        public ConnectTypeChoice ConnectType { get; set; }
        public InputData Data { get; set; }
        public MainWindow()
        {
            InitializeComponent();

            StatusBar sb = new (lblCoordinates, lblState, lblHovering);
            ShapesDao dao = new();
            IRectangleDao rectangleDao = new RectangleDao();
            
            _tools = new Tools(this, canvas, sb, dao, rectangleDao);
            _currentController = new EditController(_tools);
            
            DrawSavedData.DrawSavedRect(_currentController, _tools);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Data = new InputData();
            Data.ShowDialog();
            if (_currentController != null) _currentController.Unload();
            _currentController = new NewRectController(_tools);
        }

        private void btnConnection_Click(object sender, RoutedEventArgs e)
        {
            if (_currentController != null) _currentController.Unload();
            _currentController = new NewConnectionController(_tools);
        }
        
        private void BtnEdit_OnClick(object sender, RoutedEventArgs e)
        {
            if (_currentController != null) _currentController.Unload();
            _currentController = new EditController(_tools);
        }

        public void DisableTools()
        {
            if (_currentController != null) _currentController.Unload();
            _currentController = new EditController(_tools);
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            _tools.RectangleDao.Save(canvas);
            MessageBox.Show("Данные успешно сохранены");
        } 
    }
}