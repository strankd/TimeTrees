using System.Windows.Shapes;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.Model
{
    public sealed class ConnectionRectangle
    {
        public Rectangle Source { get; set; }
        public Rectangle Destination { get; set; }
        public Polyline Polyline { get; set; }
        public ConnectionType ConnectionType { get; set; }
    }
}