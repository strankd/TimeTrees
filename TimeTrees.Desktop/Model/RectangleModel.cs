using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Shapes;
using Newtonsoft.Json;
using TimeTrees.Core.Models;

namespace TimeTrees.DesktopGui.Model
{
    public sealed class RectangleModel
    {
        public int Id { get; set; }
        [JsonIgnore]
        public Rectangle Rectangle { get; set; }
        public double CanvasPositionX { get; set; }
        public double CanvasPositionY { get; set; }
        public List<int> Children { get; set; }
        public int Spouse { get; set; }
        [JsonIgnore]
        public TextBlock Text { get; set; }


        public RectangleModel(){}

        public RectangleModel(Rectangle rectangle)
        {
            Id = ((Person) rectangle.DataContext).Id;
            Rectangle = rectangle;
        }
    }
}