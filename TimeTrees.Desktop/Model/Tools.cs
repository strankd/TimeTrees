﻿using System.Windows.Controls;
using TimeTrees.DesktopGui.Additional_tools;
using TimeTrees.DesktopGui.DAO;

namespace TimeTrees.DesktopGui.Model
{
    sealed internal class Tools
    {
        public MainWindow MainWindow { get; }
        public Canvas Canvas { get; }
        public StatusBar StatusBar { get; }
        public ShapesDao ShapesDao { get; }
        public IRectangleDao RectangleDao { get; }

        public Tools(MainWindow mainWindow, Canvas canvas, StatusBar statusBar, ShapesDao shapesDao, IRectangleDao rectangleDao)
        {
            MainWindow = mainWindow;
            Canvas = canvas;
            StatusBar = statusBar;
            ShapesDao = shapesDao;
            RectangleDao = rectangleDao;
        }
    }
}
