using System.Windows;
using System.Windows.Shapes;
using TimeTrees.Core.Models;
using TimeTrees.Core.WorkWithDateTools;
using TimeTrees.DesktopGui.Additional_tools;
using TimeTrees.DesktopGui.DAO;
using TimeTrees.DesktopGui.Model;

namespace TimeTrees.DesktopGui.Windows
{
    public partial class EditInfo : Window
    {
        public EditInfo()
        {
            InitializeComponent();
        }

        private void BtnClose_OnClick(object sender, RoutedEventArgs e) => Close();

        private void BtnEdit_OnClick(object sender, RoutedEventArgs e)
        {
            string name = EditName.Text;
            string birthDate = EditBirth.Text;
            string deathDate = EditDeath.Text;
            
            if (DataContext is Rectangle rectangle)
            {
                if (rectangle.DataContext is Person person)
                {
                    if (CheckInputData.CheckCorrectlyInput(name, birthDate, deathDate))
                    {
                        person = new Person(person.Id, name, DateHelper.ParseDate(birthDate), deathDate != string.Empty ? DateHelper.ParseDate(deathDate) : null);
                        rectangle.DataContext = person;
                        
                        RectangleDao rectangleDao = new RectangleDao();
                        rectangleDao.Update(new RectangleModel(rectangle));
                        Close();
                        MessageBox.Show("Пользователь успешно отредактирован");
                    }
                }
            }
        }
    }
}