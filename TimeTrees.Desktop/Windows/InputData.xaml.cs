using System.Windows;
using TimeTrees.Core.Models;
using TimeTrees.Core.WorkWithDateTools;
using TimeTrees.DesktopGui.Additional_tools;

namespace TimeTrees.DesktopGui.Windows
{
    public partial class InputData
    {
        private Person _newPerson;
        public InputData()
        {
            InitializeComponent();
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            string name = InputName.Text;
            string birth = InputBirthDate.Text;
            string death = InputDeathDate.Text;
            
            if (CheckInputData.CheckCorrectlyInput(name,birth,death))
            {
                _newPerson = new Person(name, DateHelper.ParseDate(birth),
                    death != string.Empty ? DateHelper.ParseDate(death) : null);
                InputName.Text = string.Empty;
                InputBirthDate.Text = string.Empty;
                InputDeathDate.Text = string.Empty;
                Hide();
            }
        }

        public Person GetPerson() => _newPerson;
        public bool CheckPersonExist() => _newPerson != null;

        public void SetNullPerson() => _newPerson = null;
        
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e) => Hide();
    }
}