using System.Windows;

namespace TimeTrees.DesktopGui.Windows
{
    public partial class ViewRectangleInfo : Window
    {
        public ViewRectangleInfo()
        {
            InitializeComponent();
        }

        private void BtnClose_OnClick(object sender, RoutedEventArgs e) => Close();

        private void BtnEdit_OnClick(object sender, RoutedEventArgs e)
        {
            EditInfo editInfo = new EditInfo();
            editInfo.DataContext = this.DataContext;
            Hide();
            editInfo.ShowDialog();
            ShowDialog();
        }

    }
}