using System.Windows;
using System.Windows.Controls;

namespace TimeTrees.DesktopGui.Windows
{
    public partial class ConnectTypeChoice
    {
        private static string _choice;
        public ConnectTypeChoice()
        {
            InitializeComponent();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            _choice = ((RadioButton) sender).Content.ToString();
        }

        public string GetChoice() => _choice;

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e) => Hide();
    }
}