﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.Controllers
{
    sealed internal class NewRectController : Controller
    {
        private Rectangle _newRect;

        public NewRectController(Tools tools) : base(tools)
        {
            if (Tools.MainWindow.Data.CheckPersonExist())
            {
                tools.Canvas.MouseMove += OnMouseMove;
                tools.Canvas.MouseDown += OnMouseDown;
            }
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var mouseX = e.GetPosition(Tools.Canvas).X;
            var mouseY = e.GetPosition(Tools.Canvas).Y;
            var additionalInfo = GetHoveredShapesInfo();

            Tools.StatusBar.Update(State.NewElement, e.GetPosition(Tools.Canvas), additionalInfo);

            if (_newRect == null)
            {
                _newRect = new Rectangle
                {
                    Width = 50,
                    Height = 50,
                    Stroke = Brushes.Black,
                    StrokeThickness = 1.5,
                    Fill = Brushes.White,
                    DataContext = Tools.MainWindow.Data.GetPerson()
                };
                var menu = new ContextMenu();
                var mi = new MenuItem();
                mi.Height = 25;
                mi.Header = "Удалить";
                mi.Click += base.Delete;
                menu.Items.Add(mi);
                _newRect.ContextMenu = menu;
                
                Tools.Canvas.Children.Add(_newRect);
                Canvas.SetZIndex(_newRect, 2);
            }

            if (mouseX <= 3 | mouseX >= Tools.Canvas.ActualWidth - 3 | mouseY <= 3 | mouseY >= Tools.Canvas.ActualHeight - 3)
            {
                Tools.Canvas.Children.Remove(_newRect);
                _newRect = null;
            }

            if (_newRect != null)
            {
                Canvas.SetTop(_newRect, mouseY - 25);
                Canvas.SetLeft(_newRect, mouseX - 25);
            }
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Tools.RectangleDao.Add(new RectangleModel(_newRect));
            Tools.ShapesDao.AddRectangle(_newRect);
            _newRect = null;
            Tools.MainWindow.Data.SetNullPerson();
            Tools.MainWindow.DisableTools();
        }

        private void Reload()
        {
            Tools.Canvas.MouseDown -= OnMouseDown;
            Tools.Canvas.MouseDown += OnMouseDown;
        }

        public override void Unload()
        {
            Tools.Canvas.MouseMove -= OnMouseMove;
            Tools.Canvas.MouseDown -= OnMouseDown;
            Dispose();
        }
    }
}
