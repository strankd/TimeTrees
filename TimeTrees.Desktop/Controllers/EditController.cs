﻿using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using TimeTrees.Core.Models;
using TimeTrees.DesktopGui.Additional_tools;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.States;
using TimeTrees.DesktopGui.Windows;

namespace TimeTrees.DesktopGui.Controllers
{
    sealed internal class EditController : Controller
    {
        private Rectangle _selectedRect;

        public EditController(Tools tools) : base(tools)
        {
            tools.Canvas.MouseDown += OnMouseDown;
            tools.Canvas.MouseMove += OnMouseMove;
            tools.Canvas.MouseUp += OnMouseUp;
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            _selectedRect = HoveredShapes.FirstOrDefault(x => x is Rectangle) as Rectangle;
            if (_selectedRect != null)
            {
                if (e.ClickCount == 2)
                {
                    Tools.MainWindow.ViewRectangleInfo = new ViewRectangleInfo
                    {
                        DataContext = _selectedRect
                    };
                    Tools.MainWindow.ViewRectangleInfo.ShowDialog();
                }
            }
            
            Tools.StatusBar.Update(State.Edit, e.GetPosition(Tools.Canvas));
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_selectedRect != null)
            {
                Tools.Canvas.Children.Remove(_selectedRect);
                Tools.Canvas.Children.Add(_selectedRect);
                Canvas.SetZIndex(_selectedRect,2);
            }
            _selectedRect = null;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var additionalInfo = GetHoveredShapesInfo();
            Tools.StatusBar.Update(State.Edit, e.GetPosition(Tools.Canvas), additionalInfo);
            
            if (e.LeftButton == MouseButtonState.Pressed && _selectedRect != null)
            {
                var positionX = e.GetPosition(Tools.Canvas).X - _selectedRect.Width / 2;
                var positionY = e.GetPosition(Tools.Canvas).Y - _selectedRect.Height / 2;
                
                var mouseX = positionX > 3
                                    ? positionX + 50 < Tools.Canvas.ActualWidth
                                        ? positionX
                                        : Tools.Canvas.ActualWidth - 53
                                    : 3;
                
                var mouseY = positionY > 3
                                    ? positionY + 50 < Tools.Canvas.ActualHeight
                                        ? positionY
                                        : Tools.Canvas.ActualHeight - 53
                                    : 3;
                
                Canvas.SetZIndex(_selectedRect,2);
                
                Canvas.SetTop(_selectedRect, mouseY);
                Canvas.SetLeft(_selectedRect,  mouseX);

                var model = Tools.RectangleDao.Get(((Person) _selectedRect.DataContext).Id);
                
                Canvas.SetTop(model.Text,mouseY);
                Canvas.SetLeft(model.Text,mouseX - 15 - (model.Text.Text.Length * 5));

                var connections = Tools.ShapesDao.GetConnections(_selectedRect);
                
                foreach((Rectangle destination, Polyline polyline) in connections)
                {
                    ConnectionType connectionType = Tools.ShapesDao.GetConnectionType(_selectedRect,destination);
                    polyline.UpdatePolyline(_selectedRect,destination,connectionType);
                }
            }
        }

        public override void Unload()
        {
            Tools.Canvas.MouseDown -= OnMouseDown;
            Tools.Canvas.MouseMove -= OnMouseMove;
            Tools.Canvas.MouseUp -= OnMouseUp;
            Dispose();
        }
    }
}