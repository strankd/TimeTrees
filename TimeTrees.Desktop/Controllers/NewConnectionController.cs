﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.Core.Models;
using TimeTrees.DesktopGui.Additional_tools;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.States;
using TimeTrees.DesktopGui.Windows;

namespace TimeTrees.DesktopGui.Controllers
{
    sealed internal class NewConnectionController : Controller
    {
        private List<Rectangle> _selectedRects = new();
        private Polyline _newConnection;

        public NewConnectionController(Tools tools) : base(tools)
        {
            tools.Canvas.MouseMove += OnMouseMove;
            tools.Canvas.MouseDown += OnMouseDown;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var mouseX = e.GetPosition(Tools.Canvas).X;
            var mouseY = e.GetPosition(Tools.Canvas).Y;
            var additionalInfo = GetHoveredShapesInfo();

            Tools.StatusBar.Update(State.NewConnection, e.GetPosition(Tools.Canvas), additionalInfo);

            if (_selectedRects.Count == 1)
            {
                if (_newConnection == null)
                {
                    _newConnection = new Polyline
                    {
                        FillRule = FillRule.Nonzero,
                        Stroke = Brushes.White,
                        StrokeThickness = 2
                    };
                    
                    var menu = new ContextMenu();
                    var mi = new MenuItem
                    {
                        Height = 25,
                        Header = "Удалить"
                    };
                    mi.Click += base.Delete;
                    menu.Items.Add(mi);
                    _newConnection.ContextMenu = menu;

                    Tools.Canvas.Children.Add(_newConnection);
                    Canvas.SetZIndex(_newConnection, 1);
                }
                if (mouseX <= 3 | mouseX >= Tools.Canvas.ActualWidth - 3 | mouseY <= 3 | mouseY >= Tools.Canvas.ActualHeight - 3)
                {
                    Tools.Canvas.Children.Remove(_newConnection);
                    _newConnection = null;
                }
                if (_newConnection != null)
                {
                    var choiceConnectionType = Enum.Parse<ConnectionType>(Tools.MainWindow.ConnectType.GetChoice());
                    
                    var hoveredShape = GetHoveredShapes();
                    Rectangle hoveredRectangle = null;
                    foreach (var shape in hoveredShape)
                    {
                        hoveredRectangle = shape as Rectangle;
                    }
                    var selectedRect = _selectedRects.First();
                    _newConnection.UpdatePolyline(selectedRect, new Point(mouseX, mouseY), hoveredRectangle, choiceConnectionType);
                }
            }
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var hoveredRect = HoveredShapes.FirstOrDefault(x => x is Rectangle) as Rectangle;
                if (_selectedRects.Count < 2)
                {

                    if (hoveredRect == null)
                    {
                        _selectedRects = new List<Rectangle>();
                        Tools.Canvas.Children.Remove(_newConnection);
                        _newConnection = null;
                    }
                    else if (!_selectedRects.Contains(hoveredRect))
                    {
                        _selectedRects.Add(hoveredRect);
                        if (_selectedRects.Count == 1)
                        {
                            Tools.MainWindow.ConnectType = new ConnectTypeChoice();
                            Tools.MainWindow.ConnectType.ShowDialog();
                        }
                    }
                }

                if (_selectedRects.Count == 2)
                {
                    var isRectExist = Tools.ShapesDao.CheckRectExist(_selectedRects.First(), hoveredRect);
                    if (!isRectExist)
                    {
                        if (_newConnection != null)
                        {
                            var choiceConnectionType = Enum.Parse<ConnectionType>(Tools.MainWindow.ConnectType.GetChoice());

                            if (choiceConnectionType == ConnectionType.Spouses &&
                                (!Tools.RectangleDao.CheckSpouseExist(((Person) _selectedRects[0].DataContext).Id)
                                 && !Tools.RectangleDao.CheckSpouseExist(((Person) _selectedRects[1].DataContext).Id))
                                || choiceConnectionType == ConnectionType.Parent)
                            {
                                if (choiceConnectionType == ConnectionType.Parent &&
                                    !Tools.RectangleDao.CheckParentCount(((Person) _selectedRects[1].DataContext).Id))
                                {
                                    MessageBox.Show("Не может быть больше двух родителей");
                                    _selectedRects.Remove(hoveredRect);
                                    return;
                                }

                                if (choiceConnectionType == ConnectionType.Parent && Tools.RectangleDao.CheckCorrectlyAge(_selectedRects[1],_selectedRects[0]))
                                {
                                    MessageBox.Show("Дата рождения родителя не может быть меньше даты рождения ребенка");
                                    _selectedRects.Remove(hoveredRect);
                                    return;
                                }
                                if(choiceConnectionType == ConnectionType.Parent)
                                    Tools.RectangleDao.AddParent(_selectedRects[0].DataContext as Person, ((Person) _selectedRects[1].DataContext).Id);
                                
                                _newConnection.UpdatePolyline(_selectedRects[0], _selectedRects[1], choiceConnectionType);
                                Tools.ShapesDao.AddConnection(_selectedRects[0], _selectedRects[1], _newConnection, choiceConnectionType);
                                Tools.RectangleDao.AddConnect(_selectedRects[0], _selectedRects[1], choiceConnectionType);
                                _newConnection = null;
                                _selectedRects = new List<Rectangle>();
                                Reload();
                            }
                            else
                            {
                                MessageBox.Show("Не может быть больше одного супруга");
                                _selectedRects.Remove(hoveredRect);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Связь с этим человеком уже существует");
                        _selectedRects.Remove(hoveredRect);
                    }
                }
            }
        }

        void Reload()
        {
            Tools.Canvas.MouseDown -= OnMouseDown;
            Tools.Canvas.MouseDown += OnMouseDown;
        }

        public override void Unload()
        {
            Tools.Canvas.MouseMove -= OnMouseMove;
            Tools.Canvas.MouseDown -= OnMouseDown;
            Dispose();
        }
    }
}