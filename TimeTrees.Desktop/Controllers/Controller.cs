﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using TimeTrees.Core.Models;
using TimeTrees.DesktopGui.Model;

namespace TimeTrees.DesktopGui.Controllers
{
    abstract class Controller : IDisposable
    {
        protected readonly Tools Tools;
        protected List<Shape> HoveredShapes;

        protected Controller(Tools tools)
        {
            Tools = tools;
            tools.Canvas.MouseMove += BaseOnMouseMove;
            HoveredShapes = new List<Shape>();
        }

        private void BaseOnMouseMove(object sender, MouseEventArgs e)
        {
            HoveredShapes = GetHoveredShapes();
            DrawHoverEffect(HoveredShapes);
            
        }

        protected string GetHoveredShapesInfo()
        {
            return string.Join(", ", HoveredShapes.Select(x => x.GetType().Name));
        }

        private void DrawHoverEffect(List<Shape> hoveredShapes)
        {
            ClearHoverEffect();

            foreach (Shape shape in hoveredShapes)
            {
                if(shape is Rectangle rectangle)
                {
                    rectangle.Fill = Brushes.Black;
                    rectangle.Effect = new DropShadowEffect
                    {
                        Color = Colors.White,
                        ShadowDepth = 0,
                        Direction = 0,
                        BlurRadius = 25
                    };
                }
                else
                {
                    var line = (Polyline) shape;
                    line.Effect = new DropShadowEffect
                    {
                        Color = Colors.White,
                        ShadowDepth = 0,
                        Direction = 0,
                        BlurRadius = 25
                    };
                }
            }
        }

        protected void ClearHoverEffect()
        {
            foreach (var element in Tools.Canvas.Children)
            {
                if (element is Shape shape)
                {
                    if (shape is Rectangle rectangle)
                    {
                        rectangle.Fill = Brushes.White;
                        rectangle.Effect = null;
                    }
                    else
                    {
                        var line = (Polyline) shape;
                        line.Effect = null;
                    }
                }
            }
        }

        protected List<Shape> GetHoveredShapes()
        {
            List<Shape> hoveredShapes = new List<Shape>();
            foreach (var child in Tools.Canvas.Children)
            {
                if (child is Shape shape)
                {
                    if (shape.IsMouseOver)
                    {
                        hoveredShapes.Add(shape);
                    }
                }
            }
            // List<Shape> hoveredShapes = Tools.Canvas.Children.Cast<>().Where(x => x is Shape shape && shape.IsMouseOver).ToList();
            return hoveredShapes;
        }

        public abstract void Unload();

        public void Dispose()
        {
            Tools.Canvas.MouseMove -= BaseOnMouseMove;
        }

        public void Delete(object sender, RoutedEventArgs args)
        {
            UIElement shape = ((ContextMenu)((MenuItem)sender).Parent).PlacementTarget;
            
            if (shape is Rectangle rectangle)
            {
                var rectModel = Tools.RectangleDao.Get(((Person) rectangle.DataContext).Id);
                var connection = Tools.ShapesDao.GetConnections(rectangle);
                foreach (var (rect,connect) in connection)
                {
                    Tools.Canvas.Children.Remove(connect);
                }
                Tools.RectangleDao.Delete(((Person) rectangle.DataContext).Id);
                Tools.ShapesDao.DeleteRectangle(rectModel.Rectangle);
                Tools.Canvas.Children.Remove(rectangle);
                Tools.Canvas.Children.Remove(rectModel.Text);
            }
            else
            {
                var line = (Polyline) shape;
                var connection = Tools.ShapesDao.GetConnection(line);
                
                Tools.RectangleDao.DeleteConnection(connection.Source, connection.Destination, connection.ConnectionType);
                Tools.ShapesDao.DeletePolyline(line);
                Tools.Canvas.Children.Remove(line);
            }
        }
    }
}
