﻿namespace TimeTrees.DesktopGui.States
{
    public enum State
    {
        Edit,
        NewElement,
        NewConnection
    }
}
