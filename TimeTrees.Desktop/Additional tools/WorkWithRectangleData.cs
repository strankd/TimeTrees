using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Newtonsoft.Json;
using TimeTrees.Core.Models;
using TimeTrees.Core.ReadTools;
using TimeTrees.DesktopGui.Model;

namespace TimeTrees.DesktopGui.Additional_tools
{
    public static class WorkWithRectangleData
    {
        public static List<RectangleModel> ReadData()
        {
            ReadDataJson readPeople = new ReadDataJson();
            List<Person> personList = new List<Person>();
            personList = readPeople.ReadPersonFile();

            List<RectangleModel> rectangleModelsList = ReadRectangleFile();

            return JoinRectangleList(rectangleModelsList,personList);
        }

        private static List<RectangleModel> JoinRectangleList(List<RectangleModel> rectangleModels, List<Person> persons)
        {
            for (int i = 0; i < persons.Count; i++)
            {
                var person = persons[i];
                var rect = new Rectangle
                {
                    DataContext = person
                };

                if (i > rectangleModels.Count - 1)
                {
                    RectangleModel model = new RectangleModel()
                    {
                        Id = person.Id,
                        Rectangle = rect,
                        CanvasPositionX = new Random().Next(360,1080),
                        CanvasPositionY = new Random().Next(180,520),
                        Text = new TextBlock(){Text = person.Name}
                    };
                    if (person.FirstParent != null | person.SecondParent != null)
                    {
                        if (persons[i].FirstParent != null)
                        {
                            var rectParent = rectangleModels.First(x => x.Id == person.FirstParent.Id);
                            rectParent.Children ??= new List<int>();
                            rectParent.Children.Add(model.Id);
                        }
                        else if (persons[i].SecondParent != null)
                        {
                            var rectParent = rectangleModels.First(x => x.Id == person.SecondParent.Id);
                            rectParent.Children ??= new List<int>();
                            rectParent.Children.Add(model.Id);
                        }
                    }
                    rectangleModels.Add(model);
                }
                else
                {
                    rectangleModels[i].Id = persons[i].Id;
                    rectangleModels[i].Rectangle = rect;
                    rectangleModels[i].Text = new TextBlock()
                    {
                        FontSize = 20,
                        Foreground = Brushes.White,
                        Text = $"{person.Id}) {person.Name}\n{person.BirthDate.ToString("yyyy/MM/dd")}"
                    };
                }
            }
            return rectangleModels;
        }
        private static List<RectangleModel> ReadRectangleFile()
        {
            string path = "../../../../files/rectangle.json";
            ReadDataJson.CheckFileExistOrGenerateIt(path);
            string fileText = File.ReadAllText(path);
            List<RectangleModel> rectangleModelsList =
                JsonConvert.DeserializeObject<RectangleModel[]>(fileText).ToList();
            return rectangleModelsList;
        }

        public static void WriteRectDaoInFile(List<RectangleModel> list)
        {
            string path = "../../../../files/rectangle.json";
            string fileText = JsonConvert.SerializeObject(list, Formatting.Indented);
            File.WriteAllText(path,fileText);
        }
        
    }
}