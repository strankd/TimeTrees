using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using TimeTrees.DesktopGui.Controllers;
using TimeTrees.DesktopGui.Model;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.Additional_tools
{
    static internal class DrawSavedData
    {
        public static void DrawSavedRect(Controller controller, Tools args)
        {
            foreach (var model in args.RectangleDao.GetAll())
            {
                var rect = model.Rectangle;
                rect.Width = 50;
                rect.Height = 50;
                rect.Stroke = Brushes.Black;
                rect.StrokeThickness = 1.5;
                rect.Fill = Brushes.White;
                
                var menu = new ContextMenu();
                var mi = new MenuItem();
                mi.Height = 25;
                mi.Header = "Удалить";
                mi.Click += controller.Delete;
                menu.Items.Add(mi);
                rect.ContextMenu = menu;
                
                args.Canvas.Children.Add(rect);
                Canvas.SetTop(rect, model.CanvasPositionY);
                Canvas.SetLeft(rect, model.CanvasPositionX);
                Canvas.SetZIndex(rect, 2);
                
                args.Canvas.Children.Add(model.Text);
                Canvas.SetLeft(model.Text,model.CanvasPositionX - 15 - (model.Text.Text.Length * 5));
                Canvas.SetTop(model.Text,model.CanvasPositionY);
            }
            DrawConnection(controller, args);
        }

        private static void DrawConnection(Controller controller, Tools args)
        {
            foreach (var model in args.RectangleDao.GetAll())
            {
                if (model.Children == null) continue;
                
                foreach (var child in model.Children)
                {
                    var destinationChildren = args.RectangleDao.Get(child);
                    
                    Polyline connectionChildren = new Polyline();
                    connectionChildren.FillRule = FillRule.Nonzero;
                    connectionChildren.Stroke = Brushes.White;
                    connectionChildren.StrokeThickness = 2;
                    
                    var menu = new ContextMenu();
                    var item = new MenuItem();
                    item.Height = 25;
                    item.Header = "Удалить";
                    item.Click += controller.Delete;
                    menu.Items.Add(item);
                    connectionChildren.ContextMenu = menu;
                    
                    connectionChildren.UpdatePolyline(model.Rectangle,destinationChildren.Rectangle,ConnectionType.Parent);

                    args.Canvas.Children.Add(connectionChildren);
                    Canvas.SetZIndex(connectionChildren, 1);
                    
                    args.ShapesDao.AddConnection(model.Rectangle,destinationChildren.Rectangle, connectionChildren, ConnectionType.Parent);
                }
                
                if(model.Spouse == 0) continue;
                
                RectangleModel destinationSpouse = args.RectangleDao.Get(model.Spouse);
                
                Polyline connectionSpouse = new Polyline();
                connectionSpouse.FillRule = FillRule.Nonzero;
                connectionSpouse.Stroke = Brushes.White;
                connectionSpouse.StrokeThickness = 2;
                
                var spouseContextMenu = new ContextMenu();
                var menuItem = new MenuItem();
                menuItem.Height = 25;
                menuItem.Header = "Удалить";
                menuItem.Click += controller.Delete;
                spouseContextMenu.Items.Add(menuItem);
                connectionSpouse.ContextMenu = spouseContextMenu;
                
                connectionSpouse.UpdatePolyline(model.Rectangle, destinationSpouse.Rectangle, ConnectionType.Spouses);

                if (!args.ShapesDao.CheckRectExist(model.Rectangle, destinationSpouse.Rectangle))
                {
                    args.ShapesDao.AddConnection(model.Rectangle, destinationSpouse.Rectangle, connectionSpouse, ConnectionType.Spouses);
                    args.Canvas.Children.Add(connectionSpouse);
                    Canvas.SetZIndex(connectionSpouse, 1);
                }
            }
        }
    }
}