﻿using System.Windows;
using System.Windows.Controls;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.Additional_tools
{
    internal class StatusBar
    {
        private readonly Label _lblCoordinates;
        private readonly Label _lblCurrentState;
        private readonly Label _lblIsHover;

        public StatusBar(Label lblCoordinates, Label lblCurrentState, Label lblIsHover)
        {
            _lblCoordinates = lblCoordinates;
            _lblCurrentState = lblCurrentState;
            _lblIsHover = lblIsHover;
        }

        public void Update(State currentState, Point? point, string additionalInfo = null)
        {
            UpdateCurrentState(currentState);
            UpdateCoordinates(point);
            _lblIsHover.Content = additionalInfo;
        }

        private void UpdateCurrentState(State currentState) => _lblCurrentState.Content = currentState.ToString();
        private void UpdateCoordinates(Point? point)
        {
            if (point.HasValue)
            {
                _lblCoordinates.Content = $"X:{point.Value.X.ToString("000")} Y:{point.Value.Y.ToString("000")} ";
            }
        }
    }
}
