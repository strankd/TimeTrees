﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using TimeTrees.DesktopGui.States;

namespace TimeTrees.DesktopGui.Additional_tools
{
    internal static class CanvasExtenstions
    {
        private static Point Center(this Rectangle rect) => new Point(Canvas.GetLeft(rect) + rect.Width / 2, Canvas.GetTop(rect) + rect.Height / 2);

        private static (Point, Point) CenterBetweenRect(Rectangle rectangleSource, Rectangle rectangleDestination, ConnectionType connection)
        {
            var sourceRectX = Canvas.GetLeft(rectangleSource) + rectangleSource.Width / 2;
            var sourceRectY = Canvas.GetTop(rectangleSource) + rectangleSource.Height / 2;

            var destinationRectX = Canvas.GetLeft(rectangleDestination) + rectangleDestination.Width / 2;
            var destinationRectY = Canvas.GetTop(rectangleDestination) + rectangleDestination.Height / 2;

            if (connection == ConnectionType.Spouses)
            {
                var x = Math.Abs(sourceRectX - destinationRectX) / 2 + Math.Min(sourceRectX,destinationRectX);
                return (new Point(x, sourceRectY), new Point(x,destinationRectY));
            }
            var y = Math.Abs(sourceRectY - destinationRectY) / 2 + Math.Min(sourceRectY,destinationRectY);
            return (new Point(sourceRectX, y), new Point(destinationRectX,y));
        }
        public static void UpdatePolyline(this Polyline polyline, Rectangle source, Rectangle destination, ConnectionType connectionType)
        {
            (Point firstRect, Point secondRect) = CenterBetweenRect(source,destination, connectionType);
            polyline.Points.Clear();
            polyline.Points.Add(source.Center());
            polyline.Points.Add(firstRect);
            polyline.Points.Add(secondRect);
            polyline.Points.Add(destination.Center());
        }
        public static void UpdatePolyline(this Polyline polyline, Rectangle source, Point point, Rectangle destination = null, ConnectionType connectionType = ConnectionType.Parent)
        {
            polyline.Points.Clear();
            polyline.Points.Add(source.Center());
            if (destination != null)
            {
                (Point firstRect, Point secondRect) = CenterBetweenRect(source,destination, connectionType);
                polyline.Points.Add(firstRect);
                polyline.Points.Add(secondRect);
                polyline.Points.Add(destination.Center());
            }
            else
            {
                polyline.Points.Add(point);
            }
        }
    }
}
