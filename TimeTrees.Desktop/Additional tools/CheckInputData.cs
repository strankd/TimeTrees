using System;
using System.Windows;
using TimeTrees.Core.WorkWithDateTools;

namespace TimeTrees.DesktopGui.Additional_tools
{
    public static class CheckInputData
    {
        public static bool CheckCorrectlyInput(string name, string birthDate, string deathDate)
        {
            
            
            if (name == string.Empty)
            {
                MessageBox.Show("Поле \"Имя\" не может быть пустым");
                return false;
            }

            if (birthDate == string.Empty)
            {
                MessageBox.Show("Поле \"Дата рождения\" не может быть пустым");
                return false;
            }
            else
            {
                try
                {
                    DateHelper.ParseDate(birthDate);
                }
                catch (Exception e)
                {
                    MessageBox.Show($"{e.Message}\n Формат даты должен быть \"ГГГГ-ММ-ДД\"");
                    return false;
                }
            }
            if (deathDate != string.Empty)
            {
                try
                {
                    DateHelper.ParseDate(deathDate);
                }
                catch (Exception e)
                {
                    MessageBox.Show($"{e.Message}\n Формат даты должен быть \"ГГГГ-ММ-ДД\"");
                    return false;
                }
            }
            
            return true;
        }
    }
}