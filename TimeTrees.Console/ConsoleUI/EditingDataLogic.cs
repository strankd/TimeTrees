using System;
using TimeTrees.Core;
using TimeTrees.Core.Dao;
using TimeTrees.Core.Models;
using TimeTrees.Core.WorkWithDateTools;
using TimeTrees.Core.WriteTools;

namespace TimeTrees.ConsoleUI
{
    public class EditingDataLogic : ICommand
    {
        public void Execute()
        {
            PersonDao personDao = Program.PersonDao;
            
            Console.WriteLine("Для редактирования данных выберете человека из списка");
            Person editPerson = FindInListLogic.FindInList();
            
            if(editPerson == null) return;
            ConsoleHelper.ClearScreen();
            Console.CursorVisible = true;
            
            Console.WriteLine("Для удаления человека нажмите D");
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            if (keyInfo.Key == ConsoleKey.D)
            {
                personDao.Delete(editPerson.Id);
                WriteData.WritePeopleInFile(personDao.GetAll(), Program.Format);
                Console.CursorVisible = false;
                return;
            }

            string first = editPerson.FirstParent != null ? editPerson.FirstParent.Name : "нет";
            string second = editPerson.SecondParent != null ? editPerson.SecondParent.Name : "нет";
            Console.WriteLine($"Вы выбрали: {editPerson.Name}\tродитель: {first}\tродитель: {second}\t{editPerson.BirthDate.ToString("yyyy-MM-dd")}\t{editPerson.DeathDate}");
            
            Console.WriteLine("Введите новое имя или нажмите Enter для продолжения: ");
            string newname = Console.ReadLine();
            editPerson.Name = string.IsNullOrEmpty(newname)
                ? editPerson.Name
                : newname;
                
            Console.WriteLine("Введите новую дату рождения (год-месяц-день) или нажмите Enter для продолжения: ");
            string newBirthDate = Console.ReadLine();
            editPerson.BirthDate = string.IsNullOrEmpty(newBirthDate)
                ? editPerson.BirthDate
                : DateHelper.CheckBirthDateCorectly(newBirthDate);
            
            Console.WriteLine("Введите новую дату смерти (год-месяц-день), введите \"-\" для удаления текущей даты смерти или нажмите Enter для продолжения: ");
            string newDeathDate = Console.ReadLine();
            editPerson.DeathDate = string.IsNullOrEmpty(newDeathDate)
                ? editPerson.DeathDate
                : DateHelper.CheckDeathDateCorectly(newDeathDate,editPerson.BirthDate);
            
            Console.WriteLine("Чтобы изменить первого родителя человека нажмие F, введите \"-\" чтобы удалить родителя или нажмите Enter чтобы продолжить");
            editPerson.FirstParent = FindNewParent(editPerson);
            
            Console.WriteLine("Чтобы изменить второго родителя человека нажмие F, введите \"-\" чтобы удалить родителя или нажмите Enter чтобы продолжить");
            editPerson.SecondParent = FindNewParent(editPerson);
            
            personDao.Update(editPerson);
            
            WriteData.WritePeopleInFile(personDao.GetAll(), Program.Format);
            Console.WriteLine("Данные успешно изменены!");
            Console.CursorVisible = false;
            
            if(ConsoleHelper.Continue()) Execute();
        }

        static Person FindNewParent(Person editPerson)
        {
            ConsoleKeyInfo key = Console.ReadKey();
            Person person = null;
            if (key.KeyChar == '-' || key.Key == ConsoleKey.Enter)
            {
                Console.WriteLine();
            }
            else if (key.Key == ConsoleKey.F)
            {
                person = AddPersonLogic.CheckParentBirthDateCorectly(editPerson);
                ConsoleHelper.ClearScreen();
            }
            return person;
        }
    }
}