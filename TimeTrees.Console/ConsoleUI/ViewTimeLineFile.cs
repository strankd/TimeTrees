using System;
using TimeTrees.Core.Dao;

namespace TimeTrees.ConsoleUI
{
    public class ViewTimeLineFile : ICommand
    {
        public void Execute()
        {
            do
            {
                ConsoleHelper.ClearScreen();
                TimeLineDao timeLineDao = Program.TimeLineDao;
                foreach (var timelineEvent in timeLineDao.GetAll())
                {
                    Console.WriteLine($"{timelineEvent.Date.ToString("yyyy-MM-dd")}\t{timelineEvent.Event}");
                }
                Console.WriteLine("\nЧтобы вернуться назад нажмите Esc");
                ConsoleKeyInfo key = Console.ReadKey();
                if(key.Key == ConsoleKey.Escape) break;
            } while (true);
        }
    }
}