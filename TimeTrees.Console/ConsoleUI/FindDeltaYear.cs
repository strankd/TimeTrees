using System;
using TimeTrees.Core;
using TimeTrees.Core.Dao;
using TimeTrees.Core.WorkWithDateTools;

namespace TimeTrees.ConsoleUI
{
    public class FindDeltaYear : ICommand
    {
        public void Execute()
        {
            Console.Clear();
            TimeLineDao timeLineDao = Program.TimeLineDao;
            var timelinelist = timeLineDao.GetAll();
            DeltaTime deltaTime = new DeltaTime(timelinelist);
            Console.WriteLine($"Между макс и мин датами прошло: {deltaTime.Years} лет, {deltaTime.Months} месяцев и {deltaTime.Days} дней");
            if (ConsoleHelper.Continue()) Execute();
        }
    }
}