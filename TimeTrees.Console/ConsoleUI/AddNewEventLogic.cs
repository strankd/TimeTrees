using System;
using System.Collections.Generic;
using TimeTrees.Core;
using TimeTrees.Core.Dao;
using TimeTrees.Core.Models;
using TimeTrees.Core.WorkWithDateTools;
using TimeTrees.Core.WriteTools;

namespace TimeTrees.ConsoleUI
{
    public class AddNewEventLogic : ICommand
    {
        public void Execute()
        {
            Console.CursorVisible = true;
            Console.Clear();
            Console.WriteLine("Добавление нового события");
            NewEvent();
            Console.CursorVisible = false;
            if (ConsoleHelper.Continue()) Execute();
        }
        static void NewEvent()
        {
            TimelineEvent timelineEvent = new TimelineEvent();
            Console.Write("Ведите дату события: ");
            string dateEvent = Console.ReadLine();
            timelineEvent.Date = DateHelper.ParseDate(dateEvent);
            
            Console.Write("Введите описание события: ");
            timelineEvent.Event = Console.ReadLine();
            
            List<Person> participants = new List<Person>();
            Console.WriteLine("Нажмите Enter, если участники события неизвестны или кнопку F, чтобы найти участников: ");
            ConsoleKeyInfo key = Console.ReadKey();
            do
            {
                key = Console.ReadKey();
                if (key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.F)
                {
                    Console.WriteLine("Неизвестная клавиша. Введите ENTER или F.");
                }
            } while (key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.F);
            if (key.Key == ConsoleKey.F)
            {
                while (true)
                {
                    participants.Add(FindInListLogic.FindInList());
                    ConsoleHelper.ClearScreen();
                    Console.Write("Хотите ввести еще участников? [y/n]");
                    ConsoleKeyInfo answer = Console.ReadKey();
                    if (answer.Key == ConsoleKey.N) break;
                }
                ConsoleHelper.ClearScreen();
            }

            timelineEvent.Participants = participants;

            TimeLineDao timeLineDao = Program.TimeLineDao;
            timeLineDao.Add(timelineEvent);
            WriteData.WriteTimeLineEventInFile(timeLineDao.GetAll(), Program.Format);
            Console.WriteLine("Данные успешно добавлены");
        }
    }
}