﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimeTrees.Core.Dao;
using TimeTrees.Core.ReadTools;

namespace TimeTrees.ConsoleUI
{
    static class Program
    {
        public static Format Format { get; private set; }
        public static PersonDao PersonDao { get; private set; }
        public static TimeLineDao TimeLineDao { get; private set; }
        static void Main(string[] args)
        {
            Format = FileSelectionMenu.Start();
            PersonDao = new PersonDao(Format);
            TimeLineDao = new TimeLineDao(Format);
            
            Console.CursorVisible = false;
            List<MenuItem> menu = new List<MenuItem>
            {
                new MenuItem(){Execute = new FindPeopleBornInLeapYear(), Description = "поиск людей, родившихся в високосный год", Select = true},
                new MenuItem(){Execute = new FindDeltaYear(),Description = "поиск разницы макс и мин даты"},
                new MenuItem(){Execute = new AddDataMenu(),Description = "Ввод новых данных"},
                new MenuItem(){Execute = new ViewFilesMenu(),Description = "Просмотр файлов"},
                new MenuItem(){Execute = new EditingDataLogic(),Description = "Редактировать данные"},
                new MenuItem(){Description = "Exit"}
            };
            bool exit = false;
            do
            {
                ShowMenu(menu);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                        MenuNext(menu);
                        break;
                    case ConsoleKey.UpArrow:
                        MenuPrev(menu);
                        break;
                    case ConsoleKey.Enter:
                        var select = menu.Find(item => item.Select);
                        if (select.Description == "Exit") exit = true;
                        else select.Execute.Execute();
                        break;
                }
            } while (!exit);
        }
        
        public static void ShowMenu(List<MenuItem> menu)
        {
            ConsoleHelper.ClearScreen();
            foreach (var item in menu)
            {
                Console.BackgroundColor = item.Select 
                    ? ConsoleColor.DarkCyan 
                    : ConsoleColor.Black;
                Console.WriteLine(item.Description);
                Console.BackgroundColor = ConsoleColor.Black;
            }
        }
        
        public static void MenuPrev(List<MenuItem> menu)
        {
            ConsoleHelper.ClearScreen();
            MenuItem select = menu.First(item => item.Select);
            int selectindex = menu.IndexOf(select);
            menu[selectindex].Select = false;
            selectindex = selectindex == 0 
                ? menu.Count - 1 
                : --selectindex;
            menu[selectindex].Select = true;
        }
        
        public static void MenuNext(List<MenuItem> menu)
        {
            ConsoleHelper.ClearScreen();
            MenuItem select = menu.First(item => item.Select);
            int selectindex = menu.IndexOf(select);
            menu[selectindex].Select = false;
            selectindex = selectindex == menu.Count - 1 
                ? 0 
                : ++selectindex;
            menu[selectindex].Select = true;
        }
        
    }
}
