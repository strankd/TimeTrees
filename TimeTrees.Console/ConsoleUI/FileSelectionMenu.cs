using System;
using System.Collections.Generic;
using System.Linq;
using TimeTrees.Core.ReadTools;

namespace TimeTrees.ConsoleUI
{
    public static class FileSelectionMenu
    {
        public static Format Start()
        {
            Console.CursorVisible = false;
            List<FormatMenuItem> menu = new List<FormatMenuItem>
            {
                new FormatMenuItem(){Description = "Использовать csv файлы",Select = true,Format = Format.Csv},
                new FormatMenuItem(){Description = "Использовать json файлы", Format = Format.Json}
            };
            List<MenuItem> menuItems = new List<MenuItem>();
            foreach (var item in menu)
            {
                menuItems.Add(new MenuItem(){Description = item.Description, Select = item.Select});
            }
            do
            {
                Program.ShowMenu(menuItems);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    Program.MenuNext(menuItems);
                }
                if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    Program.MenuPrev(menuItems);
                }

                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    var selectedItem = menuItems.IndexOf(menuItems.First(x => x.Select));
                    menu.First(x => x.Select).Select = false;
                    menu[selectedItem].Select = true;
                    var select = menu.First(item => item.Select);
                    return select.Format;
                }
            } while (true);
        }
    }
}