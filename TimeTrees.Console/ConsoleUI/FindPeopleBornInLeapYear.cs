using System;
using TimeTrees.Core;
using TimeTrees.Core.Dao;
using TimeTrees.Core.WorkWithDateTools;

namespace TimeTrees.ConsoleUI
{
    public class FindPeopleBornInLeapYear : ICommand
    {
        public void Execute()
        {
            Console.Clear();
            PersonDao personDao = Program.PersonDao;
            var personlist = personDao.GetAll();
            LeapYear leapYear = new LeapYear(personlist);
            foreach (var person in leapYear.Persons)
            {
                Console.WriteLine($"{person.Name} родился в високосный год и его возраст не более 20 лет");
            }
            if(ConsoleHelper.Continue()) Execute();
        }
    }
}