using System;
using System.Collections.Generic;
using TimeTrees.ConsoleUI;

namespace TimeTrees
{
    public class ViewFilesMenu : ICommand
    {
        public void Execute()
        {
            List<MenuItem> viewMenu = new List<MenuItem>
            {
                new MenuItem(){Execute = new ViewPeopleFile(),Description = "Просмотр файла с людьми",Select = true},
                new MenuItem(){Execute = new ViewTimeLineFile(),Description = "Просмотр файла с событиями"},
                new MenuItem(){Description = "Назад"}
            };
            bool exit = false;
            do
            {
                Program.ShowMenu(viewMenu);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                        Program.MenuNext(viewMenu);
                        break;
                    case ConsoleKey.UpArrow:
                        Program.MenuPrev(viewMenu);
                        break;
                    case ConsoleKey.Enter:
                        var select = viewMenu.Find(item => item.Select);
                        if (select.Description == "Назад") exit = true;
                        else select.Execute.Execute();
                        break;
                }
            } while (!exit);
        }
    }
}