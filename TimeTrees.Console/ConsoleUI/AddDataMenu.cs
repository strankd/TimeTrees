using System;
using System.Collections.Generic;

namespace TimeTrees.ConsoleUI
{
    public class AddDataMenu : ICommand
    {
        public void Execute()
        {
            List<MenuItem> findMenu = new List<MenuItem>
            {
                new MenuItem(){Execute = new AddPersonLogic(),Description = "Ввести новых людей", Select = true},
                new MenuItem(){Execute = new AddNewEventLogic(),Description = "Ввести новое событие"},
                new MenuItem(){Description = "Назад"}
            };
            bool exit = false;
            do
            {
                Program.ShowMenu(findMenu);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                        Program.MenuNext(findMenu);
                        break;
                    case ConsoleKey.UpArrow:
                        Program.MenuPrev(findMenu);
                        break;
                    case ConsoleKey.Enter:
                        var select = findMenu.Find(item => item.Select);
                        if (select.Description == "Назад") exit = true;
                        else select.Execute.Execute();
                        break;
                }
            } while (!exit);
        }
    }
}