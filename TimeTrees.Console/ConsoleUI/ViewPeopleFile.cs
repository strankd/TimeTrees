using System;
using TimeTrees.Core.Dao;

namespace TimeTrees.ConsoleUI
{
    public class ViewPeopleFile : ICommand
    {
        public void Execute()
        {
            PersonDao personDao = Program.PersonDao;
            do
            {
                ConsoleHelper.ClearScreen();
                Console.WriteLine("Id\tИмя\tРодственник\tРодственник\tДата рождения\tДата смерти\n");
                foreach (var people in personDao.GetAll())
                {
                    var first = people.FirstParent == null? "нет" : $"{people.FirstParent.Id}){people.FirstParent.Name}";
                    var second = people.SecondParent == null? "нет" : $"{people.SecondParent.Id}){people.SecondParent.Name}";
                    var deathDate = people.DeathDate == null? "" : ((DateTime)people.DeathDate).ToString("yyyy-MM-dd");
                    Console.WriteLine($"{people.Id})\t{people.Name}\t{first}\t\t{second}\t\t{people.BirthDate.ToString("yyyy-MM-dd")}\t{deathDate}");
                }
                Console.WriteLine("\nЧтобы вернуться назад нажмите Esc");
                ConsoleKeyInfo key = Console.ReadKey();
                if(key.Key == ConsoleKey.Escape) break;
            } while (true);
        }
    }
}